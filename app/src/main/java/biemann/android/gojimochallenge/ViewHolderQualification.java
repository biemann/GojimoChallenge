package biemann.android.gojimochallenge;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Typical Viewholder pattern, specifically for RecyclerView
 */
public class ViewHolderQualification extends RecyclerView.ViewHolder
{
    public LinearLayout mLinearLayoutContainer;
    public TextView mTextViewQualificationName;

    public ViewHolderQualification(View itemView)
    {
        super(itemView);
        mLinearLayoutContainer = (LinearLayout) itemView.findViewById(R.id.layout_container);
        mTextViewQualificationName = (TextView) itemView.findViewById(R.id.content);
    }

    @Override
    public String toString()
    {
        return super.toString() + " '" + mTextViewQualificationName.getText() + "'";
    }
}
