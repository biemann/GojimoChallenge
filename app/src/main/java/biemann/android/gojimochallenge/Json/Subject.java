package biemann.android.gojimochallenge.Json;

public class Subject {

    private String id;
    private String title;
    private String link;
    private String colour;

    /**
     * No args constructor for use in serialization
     *
     */
    public Subject() {
    }

    /**
     *
     * @param colour
     * @param id
     * @param title
     * @param link
     */
    public Subject(String id, String title, String link, String colour) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.colour = colour;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    public Subject withId(String id) {
        this.id = id;
        return this;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public Subject withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     *
     * @return
     * The link
     */
    public String getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    public Subject withLink(String link) {
        this.link = link;
        return this;
    }

    /**
     *
     * @return
     * The colour
     */
    public String getColour() {
        return colour;
    }

    /**
     *
     * @param colour
     * The colour
     */
    public void setColour(String colour) {
        this.colour = colour;
    }

    public Subject withColour(String colour) {
        this.colour = colour;
        return this;
    }

}
