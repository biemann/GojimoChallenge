
package biemann.android.gojimochallenge.Json;

import java.util.HashMap;
import java.util.Map;

public class Info {

    private String meta;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Info() {
    }

    /**
     * 
     * @param meta
     */
    public Info(String meta) {
        this.meta = meta;
    }

    /**
     * 
     * @return
     *     The meta
     */
    public String getMeta() {
        return meta;
    }

    /**
     * 
     * @param meta
     *     The meta
     */
    public void setMeta(String meta) {
        this.meta = meta;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
