
package biemann.android.gojimochallenge.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonFeedObject {

    private String id;
    private String name;
    private Country country;
    private List<Subject> subjects = new ArrayList<Subject>();
    private String link;
    private List<DefaultProduct> defaultProducts = new ArrayList<DefaultProduct>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public JsonFeedObject() {
    }

    /**
     * 
     * @param id
     * @param subjects
     * @param link
     * @param name
     * @param defaultProducts
     * @param country
     */
    public JsonFeedObject(String id, String name, Country country, List<Subject> subjects, String link, List<DefaultProduct> defaultProducts) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.subjects = subjects;
        this.link = link;
        this.defaultProducts = defaultProducts;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The country
     */
    public Country getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(Country country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The subjects
     */
    public List<Subject> getSubjects() {
        return subjects;
    }

    /**
     * 
     * @param subjects
     *     The subjects
     */
    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    /**
     * 
     * @return
     *     The link
     */
    public String getLink() {
        return link;
    }

    /**
     * 
     * @param link
     *     The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * 
     * @return
     *     The defaultProducts
     */
    public List<DefaultProduct> getDefaultProducts() {
        return defaultProducts;
    }

    /**
     * 
     * @param defaultProducts
     *     The default_products
     */
    public void setDefaultProducts(List<DefaultProduct> defaultProducts) {
        this.defaultProducts = defaultProducts;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
