package biemann.android.gojimochallenge;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.ArrayList;

import biemann.android.gojimochallenge.Json.JsonFeedObject;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ActivityItemDetail} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ActivityItemList extends AppCompatActivity
{
    // Constants
    private final String TAG = this.getClass().getSimpleName();
    private final String KEY_RECYCLER_STATE = "recycler_state";

    // View references
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    // Member Variables
    private ApplicationMain mApplicationMain;
    private BroadcastReceiver mBroadcastReceiver;
    private AdapterQualificationsRecyclerView mAdapterQualificationsRecyclerView;
    private ArrayList<JsonFeedObject> jsonFeedObjectList;

    private static Bundle mBundleRecyclerViewState;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        // Toolbar config
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mApplicationMain = (ApplicationMain)getApplication();
        boolean bTwoPane = false;
        if (findViewById(R.id.item_detail_container) != null)
        {
            // The detail container view will be present only in the large-screen layouts (res/values-w900dp).
            // If this view is present, then the activity will be in two-pane mode.
            bTwoPane = true;
        }

        // Swipe to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                // Refresh items
                refreshItems();
            }
        });


        // Assign Adapter with RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));//do this before mRecyclerView.setAdapter()
        jsonFeedObjectList = mApplicationMain.getJsonFeedObjectList();
        mAdapterQualificationsRecyclerView = new AdapterQualificationsRecyclerView(this, jsonFeedObjectList, bTwoPane);
        mRecyclerView.setAdapter(mAdapterQualificationsRecyclerView);

        // Used to receive messages when async data is ready to show
        mBroadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                if (intent.getExtras().containsKey(ApplicationMain.EXTRA_ACTION_DATA_UPDATED))
                {
                    Log.d(TAG, "Broadcast received: Data Updated");
                    onItemsLoadComplete();
                }
                if (intent.getExtras().containsKey(ApplicationMain.EXTRA_ACTION_DATA_NOT_UPDATED))
                {
                    Log.d(TAG, "Broadcast received: Data Not Updated");
                    stopRefreshAnimation();
                }
            }
        };

        // Register broadcast receivers that are later unregistered in onDestroy()
        LocalBroadcastManager.getInstance(mApplicationMain).registerReceiver(mBroadcastReceiver, new IntentFilter(ApplicationMain.BROADCAST_NEW_JSON_DATA_READY));
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        LocalBroadcastManager.getInstance(mApplicationMain).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        // save RecyclerView scroll position
        mBundleRecyclerViewState = new Bundle();
        Parcelable listState = mRecyclerView.getLayoutManager().onSaveInstanceState();
        mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // return the mRecyclerView back to it's position before rotation took place
        if (mBundleRecyclerViewState != null) {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            mRecyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    void refreshItems()
    {
        mApplicationMain.asyncJsonGetFromServer();
    }

    void onItemsLoadComplete()
    {
        jsonFeedObjectList = mApplicationMain.getJsonFeedObjectList();
        mAdapterQualificationsRecyclerView.notifyDataSetChanged();

        // Stop refresh animation
        stopRefreshAnimation();
    }

    void stopRefreshAnimation()
    {
        mSwipeRefreshLayout.setRefreshing(false);
    }

}
