package biemann.android.gojimochallenge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import biemann.android.gojimochallenge.Json.JsonFeedObject;

/**
 * Adapter to be used with ItemListActivity.mRecyclerView
 */
public class AdapterQualificationsRecyclerView extends RecyclerView.Adapter<ViewHolderQualification>
{
    private AppCompatActivity mAppCompatActivity;
    ArrayList<JsonFeedObject> mJsonFeedObjectList;
    private boolean mTwoPane;

    // Constructor
    public AdapterQualificationsRecyclerView(AppCompatActivity compatActivity, ArrayList<JsonFeedObject> jsonFeedObjectList, boolean twoPane)
    {
        mAppCompatActivity = compatActivity;
        mJsonFeedObjectList = jsonFeedObjectList;
        mTwoPane = twoPane;
    }

    @Override
    public ViewHolderQualification onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        // NOTE: item_list_content layout is shared with AdapterSubjectsRecyclerView
        View inflatedView = LayoutInflater.from(mAppCompatActivity).inflate(R.layout.item_list_content, viewGroup, false);
        return new ViewHolderQualification(inflatedView);
    }

    @Override
    public void onBindViewHolder(final ViewHolderQualification viewHolder, int position)
    {
        String strQualificationName = getItem(position).getName();

        viewHolder.mTextViewQualificationName.setText(strQualificationName);
        viewHolder.mTextViewQualificationName.setTag(position);//important in onClick()
        viewHolder.mLinearLayoutContainer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (mTwoPane)
                {
                    Bundle arguments = new Bundle();
                    arguments.putInt(FragmentItemDetail.ARG_ITEM_ID, (Integer) viewHolder.mTextViewQualificationName.getTag());
                    FragmentItemDetail fragment = new FragmentItemDetail();
                    fragment.setArguments(arguments);
                    mAppCompatActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else
                {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, ActivityItemDetail.class);
                    intent.putExtra(FragmentItemDetail.ARG_ITEM_ID, (Integer) viewHolder.mTextViewQualificationName.getTag());
                    context.startActivity(intent);
                }
            }
        });
    }


    public JsonFeedObject getItem(int position)
    {
        return mJsonFeedObjectList.get(position);
    }

    @Override
    public int getItemCount()
    {
        return mJsonFeedObjectList.size();
    }

}