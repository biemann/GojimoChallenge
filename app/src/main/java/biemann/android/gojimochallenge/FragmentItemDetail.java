package biemann.android.gojimochallenge;

import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import biemann.android.gojimochallenge.Json.JsonFeedObject;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in :
 *  a {@link ActivityItemList} in two-pane mode (on tablets)
 *  or a {@link ActivityItemDetail} (on handsets)
 */
public class FragmentItemDetail extends Fragment
{
    // Constants
    public static final String ARG_ITEM_ID = "item_id";//argument representing the item ID

    // View references
    private TextView mTextView;
    private RecyclerView mRecyclerView;

    // Member Variables
    private ApplicationMain mApplicationMain;
    private JsonFeedObject feedObject;

    // Mandatory empty constructor
    public FragmentItemDetail()
    {}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID))
        {
            // Load the content
            mApplicationMain = (ApplicationMain)getActivity().getApplication();
            feedObject = mApplicationMain.getJsonFeedObject(getArguments().getInt(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        mTextView = (TextView) rootView.findViewById(R.id.error_message);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.itemsRecyclerView);

        // Show the detail content
        if (feedObject != null)
        {
            if (feedObject.getSubjects().size() == 0)
            {
                // if there are no subjects then show a message instead of list
                mRecyclerView.setVisibility(View.GONE);
                mTextView.setVisibility(View.VISIBLE);
                mTextView.setText(R.string.msg_no_subjects);
            }
            else
            {
                // show list of subjects
                mRecyclerView.setAdapter(new AdapterSubjectsRecyclerView(getActivity(), feedObject.getSubjects()));
            }
        }
        else
        {
            // show message that there was an error fetching content (should never happen)
            mRecyclerView.setVisibility(View.GONE);
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText(R.string.msg_error_content);
        }

        return rootView;
    }
}
