package biemann.android.gojimochallenge;

import android.app.Application;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;

import biemann.android.gojimochallenge.Json.JsonFeedObject;

/**
 * This class handles the long-term data state and makes it available to any activity
 */
public class ApplicationMain extends Application implements DataStore.AsyncServerRequestCallback
{
    // Constants
    private final String TAG = this.getClass().getSimpleName();
    public static final String BROADCAST_NEW_JSON_DATA_READY = "json.data.ready";
    public static final String EXTRA_ACTION_DATA_UPDATED = "action.extra.update.successful";
    public static final String EXTRA_ACTION_DATA_NOT_UPDATED = "action.extra.update.failed";

    // Member Variables
    private DataStore mDataStore;

    // once this callback is triggered, we have a context
    @Override
    public void onCreate()
    {
        super.onCreate();

        mDataStore = DataStore.getInstance();
        mDataStore.initialize(this, this);
        // at this point, mDataStore will have data ready to show
    }

    public ArrayList<JsonFeedObject> getJsonFeedObjectList()
    {
        return mDataStore.getJsonFeedObjectList();
    }

    public JsonFeedObject getJsonFeedObject(int index)
    {
        return mDataStore.getJsonFeedObject(index);
    }

    public void asyncJsonGetFromServer()
    {
        mDataStore.asyncJsonGetRequest();
    }

    /**
     * concrete implementation of DataStore.AsyncServerRequestCallback
     */
    @Override
    public void dataRequestFailed()
    {
        // Broadcast message that data request failed
        // this is a decoupled approach
        Intent intent = new Intent(BROADCAST_NEW_JSON_DATA_READY);
        intent.putExtra(EXTRA_ACTION_DATA_NOT_UPDATED, "");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        Log.d(TAG, "Broadcasting dataRequestFailed()");
    }

    @Override
    public void dataRequestSuccessful()
    {
        // Broadcast message that data request was successful
        // this is a decoupled approach
        Intent intent = new Intent(BROADCAST_NEW_JSON_DATA_READY);
        intent.putExtra(EXTRA_ACTION_DATA_UPDATED, "");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        Log.d(TAG, "Broadcasting dataRequestSuccessful()");
    }
}