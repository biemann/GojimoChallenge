package biemann.android.gojimochallenge.Json;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileReader;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

import biemann.android.gojimochallenge.R;

/**
 * Class that reads the Json and generates java objects
 */
public class JsonFeedObjectReader
{
    // members
    private Gson mGson;
    private String TAG = getClass().getSimpleName();

    // constructor
    public JsonFeedObjectReader() {
        mGson = new Gson();
    }

    /**
     * read JSON from res/raw and return it as a handy ArrayList
     * @param context Activity context
     * @return ArrayList<JsonFeedObject> or NULL on error
     */
    public ArrayList<JsonFeedObject> getJsonFeedObjectListFromResource(Context context) {

        ArrayList<JsonFeedObject> returnData = null;

        try {
            InputStream is = context.getResources().openRawResource(R.raw.json);
            byte[] buffer = new byte[is.available()];
            while (is.read(buffer) != -1);
            String strJson = new String(buffer);

            Type collectionType = new TypeToken<ArrayList<JsonFeedObject>>(){}.getType();
            returnData = mGson.fromJson(strJson, collectionType);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return returnData;
    }

    /**
     * read JSON from res/raw and return it as a handy ArrayList
     * @param context Activity context
     * @return ArrayList<JsonFeedObject> or NULL on error
     */
    public ArrayList<JsonFeedObject> getJsonFeedObjectListFromString(Context context, String strJson) {

        ArrayList<JsonFeedObject> returnData = null;

        try {
            Type collectionType = new TypeToken<ArrayList<JsonFeedObject>>(){}.getType();
            returnData = mGson.fromJson(strJson, collectionType);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return returnData;
    }
}
