package biemann.android.gojimochallenge;

import android.support.design.widget.CollapsingToolbarLayout;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.FrameLayout;

public class ActivityItemDetailTests extends ActivityInstrumentationTestCase2<ActivityItemDetail>
{
    // Constructor
    public ActivityItemDetailTests()
    {
        super(ActivityItemDetail.class);
    }

    // Test that activity exists and is created with invalid input
    public void testActivityExists()
    {
        ActivityItemDetail activity = getActivity();
        assertNotNull(activity);
    }

    // Test that Toolbar exists
    public void testToolbarExists()
    {
        ActivityItemDetail activity = getActivity();
        final CollapsingToolbarLayout toolbar = (CollapsingToolbarLayout)activity.findViewById(R.id.toolbar_layout);
        assertNotNull(toolbar);
    }

    // Test that Detail Container exists
    public void testDetailContainerExists()
    {
        ActivityItemDetail activity = getActivity();
        FrameLayout frameLayout = (FrameLayout)activity.findViewById(R.id.item_detail_container);
        assertNotNull(frameLayout);
    }


}
