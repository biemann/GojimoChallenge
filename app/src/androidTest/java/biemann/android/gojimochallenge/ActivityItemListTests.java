package biemann.android.gojimochallenge;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.test.ActivityInstrumentationTestCase2;
import android.util.DisplayMetrics;
import android.widget.FrameLayout;

public class ActivityItemListTests extends ActivityInstrumentationTestCase2<ActivityItemList>
{
    // Constructor
    public ActivityItemListTests()
    {
        super(ActivityItemList.class);
    }

    // Test that activity exists
    public void testActivityExists()
    {
        ActivityItemList activity = getActivity();
        assertNotNull(activity);
    }

    // Test that SwipeRefreshLayout exists
    public void testSwipeRefreshLayoutExists()
    {
        ActivityItemList activity = getActivity();
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)activity.findViewById(R.id.swipeRefreshLayout);
        assertNotNull(swipeRefreshLayout);
    }

    // Test that RecyclerView exists
    public void testRecyclerViewExists()
    {
        ActivityItemList activity = getActivity();
        final RecyclerView recyclerView = (RecyclerView)activity.findViewById(R.id.itemsRecyclerView);
        assertNotNull(recyclerView);
    }

    // Test that Toolbar exists
    public void testToolbarExists()
    {
        ActivityItemList activity = getActivity();
        final Toolbar toolbar = (Toolbar)activity.findViewById(R.id.toolbar);
        assertNotNull(toolbar);
    }

    // on Tablet, test that second pane exists
    public void testItemDetailContainer()
    {
        ActivityItemList activity = getActivity();

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        if (metrics.widthPixels/(metrics.densityDpi/160f) > 900f)
        {
            final FrameLayout frameLayout = (FrameLayout) activity.findViewById(R.id.item_detail_container);
            assertNotNull(frameLayout);
        }
    }
}
