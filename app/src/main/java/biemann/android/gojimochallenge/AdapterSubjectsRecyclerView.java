package biemann.android.gojimochallenge;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import biemann.android.gojimochallenge.Json.JsonFeedObject;
import biemann.android.gojimochallenge.Json.Subject;

/**
 * Adapter to be used with ItemDetailFragment.mRecyclerView
 */
public class AdapterSubjectsRecyclerView extends RecyclerView.Adapter<ViewHolderSubject>
{

    private final String TAG = this.getClass().getSimpleName();
    private ArrayList<Subject> mList;
    private Context mContext;

    // Constructor
    public AdapterSubjectsRecyclerView(Context context, List<Subject> subjectList)
    {
        mContext = context;
        mList = (ArrayList<Subject>) subjectList;
    }

    @Override
    public ViewHolderSubject onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        // NOTE: item_list_content layout is shared with AdapterQualificationsRecyclerView
        View inflatedView = LayoutInflater.from(mContext).inflate(R.layout.item_list_content, viewGroup, false);
        return new ViewHolderSubject(inflatedView);
    }

    @Override
    public void onBindViewHolder(final ViewHolderSubject viewHolder, int position)
    {
        String strSubjectName = getItem(position).getTitle();
        String strSubjectColor = getItem(position).getColour();

        viewHolder.mTextViewSubjectName.setBackgroundResource(R.drawable.rounded_corners);

        // for extra-snazzy effect used rounded corners for textview background
        if (strSubjectColor != null && strSubjectColor.length()>0)
        {
            try
            {
                int textColor = Color.parseColor(strSubjectColor);// note: value should look like #123456
                GradientDrawable drawable = (GradientDrawable) viewHolder.mTextViewSubjectName.getBackground();
                drawable.setColor(textColor);
            } catch (IllegalArgumentException e)
            {
                Log.d(TAG, "Illegal color value: "+strSubjectColor);
            }
        }



        viewHolder.mTextViewSubjectName.setText(strSubjectName);
        // Future Note:
        //  if subjects need to be clickable at some point, setOnClickListener() here
    }


    public Subject getItem(int position)
    {
        return mList.get(position);
    }

    @Override
    public int getItemCount()
    {
        return mList.size();
    }

}