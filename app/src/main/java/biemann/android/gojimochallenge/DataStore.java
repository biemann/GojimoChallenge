package biemann.android.gojimochallenge;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import biemann.android.gojimochallenge.Json.JsonFeedObject;
import biemann.android.gojimochallenge.Json.JsonFeedObjectReader;

/**
 * Singleton
 * Handles data: provides whatever is available and is responsible for updating its data
 * Workflow:
 * 1) does file have data ?
 * 2) if no, read from /res/raw/json
 * 3) if yes, read from file
 * 4) pull-to-refresh updates data in file
 */
public final class DataStore
{
    // Constants
    private final static DataStore INSTANCE = new DataStore();
    private final String TAG = this.getClass().getSimpleName();
    private final String SHARED_PREFERENCE_ROOT = "biemann.android.gojimochallenge.sharedprefs";
    private final String SHARED_PREFERENCE_KEY_GET_REQUEST_TIMESTAMP = "get_request_timestamp";
    private final String FILE_JSON_CACHE = "biemann.android.gojimochallenge.jsonfile";
    private final String SERVER_GET_REQUEST = "https://api.gojimo.net/api/v4/qualifications";
    private final Long PERIOD_DATA_REQUEST_MINIMUM_TIME = 2 * 60 * 1000L;//2 minutes in ms
    private final int CACHE_SIZE = 1 * 1024 * 1024; // 1 MB

    // Member Variables
    private Context mContext;
    private ArrayList<JsonFeedObject> mJsonFeedObjectList;
    private JsonFeedObjectReader mJsonFeedObjectReader;
    private File mJsonCacheFile;
    private OkHttpClient mOkHttpClient;
    private Cache mCache;
    private AsyncServerRequestCallback mAsyncServerRequestCallback;
    private boolean mCurrentlyPerformingGetRequest;

    public interface AsyncServerRequestCallback
    {
        void dataRequestSuccessful();
        void dataRequestFailed();
    }

    // constructor does not allow instantiation
    private DataStore(){}

    public static DataStore getInstance()
    {
        return INSTANCE;
    }

    /**
     * Must be called before using other functions in this class
     * @param context Context
     * @param callback ref to concrete implementation of callback interface
     */
    public synchronized void initialize(Context context, AsyncServerRequestCallback callback)
    {
        mContext = context;
        mAsyncServerRequestCallback = callback;

        // OKhttpClient
        mCache = new Cache(mContext.getCacheDir(), CACHE_SIZE);
        mOkHttpClient = new OkHttpClient();
        mOkHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        mOkHttpClient.setWriteTimeout(10, TimeUnit.SECONDS);
        mOkHttpClient.setReadTimeout(10, TimeUnit.SECONDS);
        mOkHttpClient.setCache(mCache);

        if (mJsonFeedObjectReader == null)
        {
            mJsonFeedObjectReader = new JsonFeedObjectReader();
        }

        // First, initialize the cache and read Json data if it exists
        mJsonCacheFile = new File(mContext.getCacheDir(), FILE_JSON_CACHE);
        try
        {
            if (mJsonCacheFile.exists())
            {
                Log.d(TAG, "reading data from cache...");
                FileReader fileReader = new FileReader(mJsonCacheFile);
                String strJson = readJsonDataFromFile(fileReader);
                mJsonFeedObjectList = mJsonFeedObjectReader.getJsonFeedObjectListFromString(mContext, strJson);
            }
        } catch (FileNotFoundException ex)
        {
            // Do nothing. Data will be obtained from res/raw/json shipped with app
        }

        // Second, if there is no cached data, then use data shipped with app
        if (mJsonFeedObjectList == null)
        {
            Log.d(TAG, "reading data from resource...");
            mJsonFeedObjectList = mJsonFeedObjectReader.getJsonFeedObjectListFromResource(mContext);
        }
    }

    /**
     * Returns list of JSON objects - must call verifyInitialized() first
     * @return the entire list of JSON objects
     */
    public ArrayList<JsonFeedObject> getJsonFeedObjectList()
    {
        verifyInitialized();
        return mJsonFeedObjectList;
    }

    /**
     * Returns a single JSON root object - must call verifyInitialized() first
     * @param index index of item in list to obtain
     * @return a single JSON root object
     */
    public JsonFeedObject getJsonFeedObject(int index)
    {
        verifyInitialized();
        if (index >= 0 && index < mJsonFeedObjectList.size())
        {
            return mJsonFeedObjectList.get(index);
        }

        return null;
    }

    /**
     * Sends Asynchronous GET request to server
     * Success and Failure is communicated by {@link AsyncServerRequestCallback}
     */
    public void asyncJsonGetRequest()
    {
        if (!mCurrentlyPerformingGetRequest)
        {
            mCurrentlyPerformingGetRequest = true;

            // Only allow this request once every 2 minutes
            Long lTimeSinceLastRequest = System.currentTimeMillis() - getLastGetRequestTimestamp();
            if (lTimeSinceLastRequest > PERIOD_DATA_REQUEST_MINIMUM_TIME)
            {

                Request request = new Request.Builder()
                        .url(SERVER_GET_REQUEST)
                        .build();

                mOkHttpClient.newCall(request).enqueue(new Callback()
                {

                    @Override
                    public void onFailure(Request request, IOException e)
                    {
                        if (mAsyncServerRequestCallback != null)
                        {
                            mAsyncServerRequestCallback.dataRequestFailed();
                        }
                        mCurrentlyPerformingGetRequest = false;
                    }

                    @Override
                    public void onResponse(Response response) throws IOException
                    {

                        setLastSuccessfulGetRequestTimestamp(System.currentTimeMillis());

                        // Check Status header and look for 304 to indicate cached response
                        Headers responseHeaders = response.headers();
                        final String strStatus = responseHeaders.get("Status");

                        if (strStatus.contains("304"))
                        {
                            // Underlying data wasn't updated - don't spend CPU cycles on refreshing UI
                            if (mAsyncServerRequestCallback != null)
                            {
                                mAsyncServerRequestCallback.dataRequestFailed();
                            }
                            mCurrentlyPerformingGetRequest = false;
                        }
                        else
                        {
                            // cache Json data
                            final String strJsonData = response.body().string();
                            saveJsonDataToFile(strJsonData, false);

                            // callback
                            if (mAsyncServerRequestCallback != null)
                            {
                                mAsyncServerRequestCallback.dataRequestSuccessful();
                            }

                            mCurrentlyPerformingGetRequest = false;
                        }
                    }
                });
            } else
            {
                Log.d(TAG, "Not allowing GET request since last request was only " + lTimeSinceLastRequest + " ms ago");
                mCurrentlyPerformingGetRequest = false;
                if (mAsyncServerRequestCallback != null)
                {
                    mAsyncServerRequestCallback.dataRequestFailed();
                }
            }
        }
        else
        {
            Log.d(TAG, "Not allowing Get request since one is already in progress");
            if (mAsyncServerRequestCallback != null)
            {
                mAsyncServerRequestCallback.dataRequestFailed();
            }
        }
    }


    // PRIVATE UTILITY FUNCTIONS BELOW


    private void verifyInitialized() throws IllegalStateException
    {
        if (mContext == null)
        {
            throw new IllegalStateException("DataStore is not initialized");
        }
    }

    private void setLastSuccessfulGetRequestTimestamp(Long timestamp)
    {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREFERENCE_ROOT, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(SHARED_PREFERENCE_KEY_GET_REQUEST_TIMESTAMP, timestamp);
        editor.commit();
    }

    private Long getLastGetRequestTimestamp()
    {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREFERENCE_ROOT, Context.MODE_PRIVATE);
        return sharedPreferences.getLong(SHARED_PREFERENCE_KEY_GET_REQUEST_TIMESTAMP, 0);
    }

    /**
     * Save JSON file to file
     * @param data String data representing JSON
     * @param appendData true to append data
     * @return true when successful, otherwise false
     */
    private boolean saveJsonDataToFile(String data, boolean appendData)
    {
        boolean bSuccess = true;
        FileWriter fileWriter = null;
        try
        {
            fileWriter = new FileWriter(mJsonCacheFile);
            fileWriter.write(data);
            fileWriter.flush();
        } catch (IOException ex)
        {
            bSuccess = false;
        } finally
        {
            try
            {
                if (fileWriter != null)
                {
                    fileWriter.close();
                }
            } catch (IOException ex)
            {
                //nothing can be done
            }
        }
        return bSuccess;
    }

    private String readJsonDataFromFile(FileReader fileReader)
    {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            String strCurrLine;
            bufferedReader = new BufferedReader(fileReader);

            while ((strCurrLine = bufferedReader.readLine()) != null)
            {
                stringBuilder.append(strCurrLine);
            }

        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (bufferedReader != null)bufferedReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return stringBuilder.toString();
    }
}
