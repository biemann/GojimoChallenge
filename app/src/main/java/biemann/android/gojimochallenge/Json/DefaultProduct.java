
package biemann.android.gojimochallenge.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultProduct {

    private String id;
    private String title;
    private String link;
    private String iosIapId;
    private List<Object> storeIds = new ArrayList<Object>();
    private String type;
    private List<Asset> assets = new ArrayList<Asset>();
    private Publisher publisher;
    private String author;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public DefaultProduct() {
    }

    /**
     * 
     * @param id
     * @param author
     * @param assets
     * @param storeIds
     * @param title
     * @param link
     * @param type
     * @param iosIapId
     * @param publisher
     */
    public DefaultProduct(String id, String title, String link, String iosIapId, List<Object> storeIds, String type, List<Asset> assets, Publisher publisher, String author) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.iosIapId = iosIapId;
        this.storeIds = storeIds;
        this.type = type;
        this.assets = assets;
        this.publisher = publisher;
        this.author = author;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The link
     */
    public String getLink() {
        return link;
    }

    /**
     * 
     * @param link
     *     The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * 
     * @return
     *     The iosIapId
     */
    public String getIosIapId() {
        return iosIapId;
    }

    /**
     * 
     * @param iosIapId
     *     The ios_iap_id
     */
    public void setIosIapId(String iosIapId) {
        this.iosIapId = iosIapId;
    }

    /**
     * 
     * @return
     *     The storeIds
     */
    public List<Object> getStoreIds() {
        return storeIds;
    }

    /**
     * 
     * @param storeIds
     *     The store_ids
     */
    public void setStoreIds(List<Object> storeIds) {
        this.storeIds = storeIds;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The assets
     */
    public List<Asset> getAssets() {
        return assets;
    }

    /**
     * 
     * @param assets
     *     The assets
     */
    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }

    /**
     * 
     * @return
     *     The publisher
     */
    public Publisher getPublisher() {
        return publisher;
    }

    /**
     * 
     * @param publisher
     *     The publisher
     */
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    /**
     * 
     * @return
     *     The author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * 
     * @param author
     *     The author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
