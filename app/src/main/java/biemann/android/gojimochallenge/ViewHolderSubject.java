package biemann.android.gojimochallenge;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Typical Viewholder pattern, specifically for RecyclerView
 */
public class ViewHolderSubject extends RecyclerView.ViewHolder
{
    public TextView mTextViewSubjectName;

    public ViewHolderSubject(View itemView)
    {
        super(itemView);
        mTextViewSubjectName = (TextView) itemView.findViewById(R.id.content);
    }

    @Override
    public String toString()
    {
        return super.toString() + " '" + mTextViewSubjectName.getText() + "'";
    }
}
