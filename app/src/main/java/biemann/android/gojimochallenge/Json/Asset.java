
package biemann.android.gojimochallenge.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Asset {

    private String id;
    private Object copyright;
    private Object meta;
    private Integer size;
    private String contentType;
    private String createdAt;
    private String updatedAt;
    private String path;
    private Object unzippedBaseUrl;
    private List<Info> info = new ArrayList<Info>();
    private String link;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Asset() {
    }

    /**
     * 
     * @param updatedAt
     * @param id
     * @param createdAt
     * @param link
     * @param path
     * @param unzippedBaseUrl
     * @param contentType
     * @param copyright
     * @param meta
     * @param info
     * @param size
     */
    public Asset(String id, Object copyright, Object meta, Integer size, String contentType, String createdAt, String updatedAt, String path, Object unzippedBaseUrl, List<Info> info, String link) {
        this.id = id;
        this.copyright = copyright;
        this.meta = meta;
        this.size = size;
        this.contentType = contentType;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.path = path;
        this.unzippedBaseUrl = unzippedBaseUrl;
        this.info = info;
        this.link = link;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The copyright
     */
    public Object getCopyright() {
        return copyright;
    }

    /**
     * 
     * @param copyright
     *     The copyright
     */
    public void setCopyright(Object copyright) {
        this.copyright = copyright;
    }

    /**
     * 
     * @return
     *     The meta
     */
    public Object getMeta() {
        return meta;
    }

    /**
     * 
     * @param meta
     *     The meta
     */
    public void setMeta(Object meta) {
        this.meta = meta;
    }

    /**
     * 
     * @return
     *     The size
     */
    public Integer getSize() {
        return size;
    }

    /**
     * 
     * @param size
     *     The size
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * 
     * @return
     *     The contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * 
     * @param contentType
     *     The content_type
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The path
     */
    public String getPath() {
        return path;
    }

    /**
     * 
     * @param path
     *     The path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * 
     * @return
     *     The unzippedBaseUrl
     */
    public Object getUnzippedBaseUrl() {
        return unzippedBaseUrl;
    }

    /**
     * 
     * @param unzippedBaseUrl
     *     The unzipped_base_url
     */
    public void setUnzippedBaseUrl(Object unzippedBaseUrl) {
        this.unzippedBaseUrl = unzippedBaseUrl;
    }

    /**
     * 
     * @return
     *     The info
     */
    public List<Info> getInfo() {
        return info;
    }

    /**
     * 
     * @param info
     *     The info
     */
    public void setInfo(List<Info> info) {
        this.info = info;
    }

    /**
     * 
     * @return
     *     The link
     */
    public String getLink() {
        return link;
    }

    /**
     * 
     * @param link
     *     The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
