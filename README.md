# Gojimo Code Challenge

CHALLENGE REQUIREMENTS
----------------------
Develop a very simple working application on your chosen platform (Android / iOS / Web) with the following functionality:

0. Read the following JSON feed containing a list of qualifications: https://api.gojimo.net/api/v4/qualifications
0. Display a list of qualifications based on the data provided by the feed.
0. Allow users to click a qualification to be presented with a list of subjects for that qualification.

BONUS POINTS
------------
Whilst not essential, we will be very pleased to see you take the challenge further by:

* Using the data provided to its full potential by taking advantage of fields like colour for subjects.
* Making the GUI as appealing and responsive as possible.
* Storing the data locally and refresh from the server as requested, taking advantage of the HTTP headers to avoid downloads of non-stale data.
* Unit testing your code

IMPLEMENTATION
--------------
![](https://github.com/abiemann/GojimoChallenge/blob/master/app_video.gif)

FEATURE LIST
------------
* Easy UX: Qualifications List -> Click -> Subjects list
* Colored Subjects list with rounded corners
* Material Design
* Pull to Refresh of Qualifications list
* Tablet and Phone layouts
* Async data download
* Data Caching
* Data is shipped with the app so that something is available if user opens app first time without internet - this data is replaced on first GET request from server.








